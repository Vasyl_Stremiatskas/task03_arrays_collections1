class A {
    
    int[] intersection(int array1[], int array2[]) {
        int count = 0;
        for(int i:array1) {
            for(int j:array2) {
                if(i == j) {
                    count++;
                }
            }
        }
        int array3[] = new int[count];
        int pos = 0;
        for(int i:array1) {
            for(int j:array2) {
                if(i == j) {
                    array3[pos] = i;
                    pos++;
                }
            }
        }
        
        return array3;
    }
    int[] difference(int array1[], int array2[]) {
        int count = 0;
        boolean isIn = false;
        for(int i:array1) {
            isIn = false;
            for(int j:array2) {
                if(i == j) {
                    isIn = true;
                }
            }
            if(!isIn) {
                count++;
            }
        }
        
        int array3[] = new int[count];
        int pos = 0;
        for(int i:array1) {
            isIn = false;
            for(int j:array2) {
                if(i == j) {
                    isIn = true;
                }
            }
            if(!isIn) {
                array3[pos] = i;
                pos++;
            }
        }
        
        return array3;
    }
}
