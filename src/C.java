class C {
    
    public int[] noRepeat(int array[]) {
        int count = 0;
        for(int i = 0; i < array.length - 1; i++) {
            if(array[i] == array[i+1]) {
                continue;
            }
            else {
                count++;
            }
        }
        
        int result[] = new int[count];
        int pos = 0;
        for(int i = 1; i < array.length; i++) {
            if(array[i] == array[i-1]) {
                continue;
            }
            else {
                result[pos] = array[i-1];
                pos++;
            }
        }
        
        return result;
    }
}
