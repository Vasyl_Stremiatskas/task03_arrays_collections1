import java.util.*;

class D {
    int countDoors = 0;
    boolean was[];
    Hero hero = new Hero();
    ArrayList<SomeObject> objects;
    
    public D() {
        was = new boolean[10];
        objects = new ArrayList();
        for(int i = 0; i < 10; i++) {
            was[i] = false;
            if(randomBool()) {
                Random rnd = new Random(System.currentTimeMillis());
                int number = 10 + rnd.nextInt(80 - 10 + 1);
                objects.add(new MagicObject(number));
            }
            else {
                Random rnd = new Random(System.currentTimeMillis());
                int number = 5 + rnd.nextInt(100 - 5 + 1);
                objects.add(new Monster(number));
            }
        }
    }
   
    public void tryNotDie() {
        Random rnd = new Random(System.currentTimeMillis());
        int number = 0 + rnd.nextInt(9 - 0 + 1);
        if(was[number] == false) {
            was[number] = true;
            if(objects.get(number).name == "Magic") {
                System.out.println("You found magic artifact with " + objects.get(number).points + "points");
                hero.points += objects.get(number).points;
                System.out.println("Your points: " + hero.points);
                countDoors++;
                tryNotDie();
            }
            if(objects.get(number).name == "Monster") {
                if(objects.get(number).points > hero.points) {
                    System.out.println("You dead :) Number of visited doors: " + countDoors );
                    return;
                }
                else {
                    System.out.println("You fight a monster");
                    tryNotDie();
                }
            }
        }
        else {
            tryNotDie();
        }
        
    }
    private boolean randomBool() {
        Random rnd = new Random(System.currentTimeMillis());
        int number = 1 + rnd.nextInt(10);
        if(number % 2 == 0) {
            return true;
        }
        else {
            return false;
        }
    }
}