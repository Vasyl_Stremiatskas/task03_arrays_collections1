class B {
    
    int[] noRepeat(int array[]) {
     int n = array.length;

        for ( int i = 0, m = 0; i != n; i++, n = m ) {
            for ( int j = m = i + 1; j != n; j++ ) {
                if ( array[j] != array[i] ) {
                    if ( m != j ) { 
                        array[m] = array[j];
                    }
                    m++;
                }
            }
        }

        int[] result = new int[n];
        if ( n != array.length ) {
            for ( int i = 0; i < n; i++ ) {
                result[i] = array[i];
            }
            return result;
        }
        else {
            return array;
        }
    }
}
