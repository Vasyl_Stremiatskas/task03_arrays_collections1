abstract class SomeObject {
    int points;
    String name;
    
    SomeObject(int value, String name) {
        this.points = value;
        this.name = name;
    }
}
